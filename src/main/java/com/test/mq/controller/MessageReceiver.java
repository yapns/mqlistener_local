package com.test.mq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.annotation.JmsListeners;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory; 
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPMessage; 
import javax.xml.soap.SOAPConnectionFactory; 
import javax.xml.soap.SOAPException; 

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;  
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList; 
import java.util.List;
import java.util.Properties; 
import org.apache.log4j.Logger;

@Component
public class MessageReceiver implements MessageListener {
	private static final Logger logger = Logger.getLogger(MessageReceiver.class);
	@Autowired
	private JmsTemplate jmsTemplate;
	private String inputStream = "E:\\MQListener\\config\\inputXML.properties";
	private String errorWS = "E:\\MQListener\\error\\wsFailed.txt";
	private String strEndQueue = "ENDQUEUE";

	public void onMessage(Message message) {
		String received = "";
		String xml = "";
		try { 
			received = ((TextMessage) message).getText();
			logger.info("Start onMessage");
			 
			logger.info("Message queue STARTQUEUE: " + received);

			boolean isSuccessWS = false;
		  
			logger.info("Start calling WS ...");
			xml = genXML(received);
			  
			if (!xml.equals("")) {	 
				isSuccessWS = callWS(xml);
				if (isSuccessWS) {
					logger.info("Send to queue ENDQUEUE");
					jmsTemplate.convertAndSend(strEndQueue, received);
					logger.info("Send queue ENDQUEUE success!!!");
				} else {
					logger.error("Failed to process, NOT send to queue ENDQUEUE");
					errorWriteFile(xml, errorWS);
				}
			}else{ 
				logger.error("Failed to generate SOAP request message.");
				//errorWriteFile(received, errorWS);
			}
			
			List <String> errorList = getErrorList();
			 
			if(errorList.size()>0){
				logger.info("Error List ="+errorList.size());
				for(int j =0;j<errorList.size();j++){
					logger.info("Retry for error ("+ (j+1) + ") message: "+ errorList.get(j));
					retriggerErrorWS(errorList.get(j));
				} 
			}
			logger.info("End WS");
			logger.info("End WS onMessage");

		} catch (JMSException e) { 
			errorWriteFile(xml, errorWS);
			logger.error("JMSException e:" + e.toString(), e);
			e.printStackTrace();
		} 
	}
	
	private List <String> getErrorList(){
		 
		File file = new File(errorWS );
 
		BufferedReader b ;
		List <String> errorList = new ArrayList <String>();
		FileOutputStream fos = null;
		if (!file.exists()) {
			errorList = null;
		}else{
			 try {
				b = new BufferedReader(new FileReader(file));
				String readLine = "";
 
                int i=0;
	            while ((readLine = b.readLine()) != null) {
	            	errorList.add(readLine); 
	            } 
	            
	            FileWriter fwOb = new FileWriter(file, false); 
	            PrintWriter pwOb = new PrintWriter(fwOb, false);
	            pwOb.flush();
	            pwOb.close();
	            fwOb.close();
	            logger.info("error list clean!"); 
			} catch (FileNotFoundException e) {
				logger.error("File not found :" + e.toString(),e);
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		}
		return errorList;
	}

	private void errorWriteFile(String content, String strFile) {

		String strErrorFolder = strFile.substring(0, strFile.lastIndexOf("\\"));
		File file = new File(strErrorFolder);
		if (!file.exists()) {
			if (file.mkdir()) {
				logger.info("Error folder created - " + strErrorFolder);
			}
		}

		logger.info("Write to file :" + strFile +" (content="+content+")");
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(strFile, true);
			bw = new BufferedWriter(fw);
			bw.write(content + "\n");

		} catch (IOException e) {
			logger.error("IOException: %s%n", e);
		} finally {
			try {
				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				logger.error("IOException: %s%n", ex);
			}
		}
	}

	 public void retriggerErrorWS( String xml ){
	  
			if (callWS(xml)) {
				logger.info("Send to queue ENDQUEUE");
				jmsTemplate.convertAndSend("ENDQUEUE", xml);
				logger.info("Send queue success!!!");
			} else {
				logger.error("Failed to process, NOT send to queue ENDQUEUE");
				errorWriteFile(xml, errorWS);
			}
	    }
 
	 private boolean callWS(String xmlInput) {
		 
	      boolean isSuccessWS = false;
			Properties mainProperties = new Properties();
			try {
	 
				String wsEndPoint ="http://wsf.cdyne.com/WeatherWS/Weather.asmx?WSDL"; 
				xmlInput = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:weat='http://ws.cdyne.com/WeatherWS/'><soapenv:Header/><soapenv:Body><weat:GetCityForecastByZIP><weat:ZIP>sdsd</weat:ZIP></weat:GetCityForecastByZIP></soapenv:Body></soapenv:Envelope>";
			 
				URL endpoint = null ;
				try {
					endpoint = new URL(wsEndPoint);
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			 
					logger.info("SOAP Connect start...." + wsEndPoint);
					SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
					SOAPConnection soapConnection = soapConnectionFactory.createConnection();
	 
					logger.info("SOAP request xml:\n" + xmlInput);
					InputStream is = new ByteArrayInputStream(xmlInput.getBytes());
					SOAPMessage soapMessage = MessageFactory.newInstance().createMessage(null, is); 
					SOAPMessage soapResponse = soapConnection.call(soapMessage, endpoint); 
					ByteArrayOutputStream responceOutputStream = new ByteArrayOutputStream();
					soapResponse.writeTo(responceOutputStream);
					  					 
			           String strSOAPResp = responceOutputStream.toString();
			         
					
					logger.info("SOAP response s:\n" + strSOAPResp);
					
					if(strSOAPResp.contains("<result>N</result>")){
						logger.info("SOAP response return result N. ");
						  isSuccessWS = false;
					}else{
						  isSuccessWS = true;
					}
	         
				} catch (UnsupportedOperationException e) {
					logger.error("UnsupportedOperationException e:" + e.toString(), e);
					//errorWriteFile(xmlInput, errorWS);
					e.printStackTrace();
				} catch (SOAPException e) {
					logger.error("SOAPException e:" + e.toString(), e);
					//errorWriteFile(xmlInput, errorWS);
					e.printStackTrace();
				} catch (Exception e) {
					logger.error("Exception e:" + e.toString(), e); 
					//errorWriteFile(xmlInput, errorWS);
					e.printStackTrace();
				}
	 
			  return isSuccessWS;
		}
 
 
  	 
//       private static String printSOAPResponse(SOAPMessage soapResponse) throws Exception {
//	    	
//	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
//	        Transformer transformer = transformerFactory.newTransformer();
//	        Source sourceContent = soapResponse.getSOAPPart().getContent();
//	        
//	        String strOut = System.out.toString();
//	 
//	        StringWriter writer = new StringWriter();
//	        writer.write(strOut);
//	        StreamResult result = new StreamResult(writer);
//	         
//	        transformer.transform(sourceContent, result);
//	        
//	       return writer.toString().substring(strOut.length(),writer.toString().length());
//	    }
     
	private String genXML(String received) {
		  
		String strXML = "";
		Properties mainProperties = new Properties();

		String openheader = "";
		String closeheader = "";

		String openFunction = "";
		String closeFunction = "";

		String openBody = "";
		String closeBody = "";

		String openTab = "";
		String closeTab = "";
		 
		try {
			FileInputStream file = new FileInputStream(inputStream);
			mainProperties.load(file);
			file.close();
			openTab = mainProperties.getProperty("oTab");
			closeTab = mainProperties.getProperty("cTab");
			openheader = mainProperties.getProperty("oheader");
			closeheader = mainProperties.getProperty("cheader");

			openFunction = mainProperties.getProperty("oFunction");
			closeFunction = mainProperties.getProperty("cFunction");
			openBody = mainProperties.getProperty("oBody");
			closeBody = mainProperties.getProperty("cBody");
 
        
            strXML = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:weat='http://ws.cdyne.com/WeatherWS/'><soapenv:Header/><soapenv:Body><weat:GetCityForecastByZIP><weat:ZIP>sdsd</weat:ZIP></weat:GetCityForecastByZIP></soapenv:Body></soapenv:Envelope>";
          
			 
			//logger.info("SOAP request: \n"+ strXML.replace(">", ">\n"));
			logger.info("SOAP request: \n" + strXML);

		} catch (FileNotFoundException e1) {
			logger.error("Config file not exists !!!");
			e1.printStackTrace();
		} catch (IOException e) {
			logger.error("Config file failed to load !!!");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return strXML;  
	}
 
	// parse XML
	private static Document parseXmlFile(String in) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(in));
			return db.parse(is);
		} catch (IOException e) {
			logger.error("IOException e:" + e.toString());
			throw new RuntimeException(e);
		} catch (SAXException e) {
			logger.error("SAXException e:" + e.toString());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			logger.error("ParserConfigurationException e:" + e.toString());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
