package com.test.mq.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
 
import com.sun.mail.smtp.SMTPTransport;

public class EmailNotification {
	    private static final Logger logger = Logger.getLogger(EmailNotification.class);
		
	    private static String SMTP_SERVER = "";
	    private static String SMTP_AUTH  = "";
	    private static String SMTP_PORT  = "";
	    private static String SMTP_TLS   = "";
	     
	    private static String USERNAME = "";
	    private static String PASSWORD = ""; 
		 public static void setProperties(){
			 try {
					File fileProp = new File("E:\\MQListener\\config\\email.properties");
					logger.info("Loading properties at " + fileProp.getAbsolutePath());
					Properties prop = new Properties();
					prop.load(new FileInputStream(fileProp));
					SMTP_SERVER = prop.getProperty("mail.smtp.host");
					SMTP_PORT   = prop.getProperty("mail.smtp.port");
					USERNAME    = prop.getProperty("username");
					PASSWORD    = Decrypt(prop.getProperty("password")); 
					SMTP_AUTH   = prop.getProperty("mail.smtp.auth");
					SMTP_TLS    = prop.getProperty("mail.smtp.starttls.enable");
	  
					logger.info("mail.smtp.host = " + SMTP_SERVER);
					logger.info("mail.smtp.port = " + SMTP_PORT);
					logger.info("mail.smtp.auth = " + SMTP_AUTH);
					logger.info("mail.smtp.starttls.enable = " +SMTP_TLS); 
				} catch (FileNotFoundException var3) {
					logger.error("ERROR: File not found - ", var3);
				} catch (Exception var4) {
					logger.error(var4);
				}
		 }

		 public static void sendEmail(String templateID,  String case_no, String batch_id, String profile_type, String date ,String message){
			 setProperties();
			    logger.info("Sending Email ...");
				String fullPath ="E:\\MQListener\\config\\config.xml";
			  
				String SUBJECT = "subject";
				String FROM = "from";
				String TO = "to";
				String CC = "cc";
				String BCC = "bcc";
				String MESSAGE = "msg";
	                
					if (templateID != null) {
						logger.info("Sending Email for template profile: " + templateID);
						File file = new File(fullPath);
						DocumentBuilder db;
						try {
							db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
							Document doc = db.parse(file);
							XPathFactory xpathFactory = XPathFactory.newInstance();
							XPath xpath = xpathFactory.newXPath();
							NodeList mappingList = (NodeList) xpath.evaluate("//emailTemplate/template[@id='" + templateID + "']", doc, XPathConstants.NODESET);
							logger.info(mappingList.getLength() + " template found");
						 

						for (int i = 0; i < mappingList.getLength(); ++i) {
							Element template = (Element) mappingList.item(i);
							String subject;
							try {
								subject = valueForNode(template, "subject", xpath, false);
							 
							logger.info("subject is: " + subject);
							String from =  valueForNode(template, "from", xpath, false);
							logger.info("from is: " + from);
							String to =  valueForNode(template, "to", xpath, false);
							logger.info("to is: " + to);
							String cc =  valueForNode(template, "cc", xpath, true);
							logger.info("CC is: " + cc);
							String bcc = valueForNode(template, "bcc", xpath, true);
							logger.info("bcc is: " + bcc);
							String msg = valueForNode(template, "msg", xpath, false);
							logger.info("msg is: " + msg);
							subject = substituteVariable(subject, case_no, "", "","", "");
							msg = substituteVariable(msg, case_no, batch_id,profile_type, date, message); 
							logger.info("msg is: " + msg);
						   	 
					    	try {
					    		send( subject, from,  to,  cc,  bcc,   msg);
								logger.info("Message for templete " + templateID + " sent to " + to);
							} catch (Exception var25) {
								logger.info("Error in sending message for templete " + templateID + " sent to " + to);
								logger.info(var25);
								throw var25;
							} 
					    	
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						} catch (ParserConfigurationException e1) {
							logger.error("ParserConfigurationException: "+e1.toString(),e1);
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (SAXException e1) {
							logger.error("SAXException: "+e1.toString(),e1);
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							logger.error("IOException: "+e1.toString(),e1);
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (XPathExpressionException e1) {
							logger.error("XPathExpressionException: "+e1.toString(),e1);
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 
					}
		    }
		    
		 private static String valueForNode(Element ele, String nodeName, XPath xpath, boolean isMultipleValue) throws Exception {
				String rtn = "";
				List<String> mulValue = new ArrayList();
				NodeList subNodes = (NodeList) xpath.evaluate(nodeName, ele, XPathConstants.NODESET);
				if (subNodes != null && subNodes.getLength() != 0) {
					if (isMultipleValue) {
						for (int j = 0; j < subNodes.getLength(); ++j) {
							mulValue.add(((Element) subNodes.item(j)).getTextContent());
						}

						rtn = mulValue.toString().replaceAll("^\\[|\\]$", "");
					} else {
						rtn = ((Element) subNodes.item(0)).getTextContent();
					}

					return rtn;
				} else {
					return rtn;
				}
			}
			
		 public static void send(String  subject,  String from,  String to, String cc, String bcc , String message) {
				    Properties prop = System.getProperties();
			        prop.put("mail.smtp.host", SMTP_SERVER); //optional, defined in SMTPTransport
			        prop.put("mail.smtp.auth", SMTP_AUTH);
			        prop.put("mail.smtp.port", Integer.parseInt(SMTP_PORT)); // default port 25
			        prop.put("mail.smtp.starttls.enable", SMTP_TLS);
			
			        Session session = Session.getInstance(prop, null);
			        Message msg = new MimeMessage(session);
			
			        try {
					
						// from
			            msg.setFrom(new InternetAddress(from));
			
						// to 
			            msg.setRecipients(Message.RecipientType.TO,
			                    InternetAddress.parse(to, true));
			
			         // bcc
			            msg.setRecipients(Message.RecipientType.BCC,
			                    InternetAddress.parse(bcc, true));
						// cc
			            msg.setRecipients(Message.RecipientType.CC,
			                    InternetAddress.parse(cc, true));
			
						// subject
			            msg.setSubject(subject);
						
						// content 
			            msg.setText(message);
						
			            msg.setSentDate(new Date());
			
			            logger.info("Start sending mail...");
						// Get SMTPTransport
			            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
					 
						// connect
			            t.connect(SMTP_SERVER,Integer.parseInt(SMTP_PORT),  USERNAME, PASSWORD);
						
						// send
			            t.sendMessage(msg, msg.getAllRecipients());
			
			           // System.out.println("Response: " + t.getLastServerResponse());
			            logger.info("End sending mail.");
			            t.close();
			
			        } catch (MessagingException e) {
			        	logger.error("Send mail failed due to reason :"+e.toString(), e);
			            e.printStackTrace();
			        }		
			    }

		 private static String substituteVariable(String strChange, String case_no, String batch_id,String profile_type, String date, String message) {
				if (strChange == null) {
					return null;
				} else {
				    strChange = strChange.replaceAll("@case_no",case_no ); 
					strChange = strChange.replaceAll("@batch_id",batch_id ); 
					strChange = strChange.replaceAll("@profile_type",profile_type );
					strChange = strChange.replaceAll("@dateTime",date );
					strChange = strChange.replaceAll("@message",message );    
	 
					return strChange;
				}
			}

			public static String Decrypt(String Password) {
				 
				StringBuffer strNew = new StringBuffer();
				StringBuffer strTemp = new StringBuffer(Password);
				int intPwd = 0;
				int i = 0;
				int intTemp = 0;

				String decrypt = "";

				try { 
					intPwd = 99 - Integer.parseInt(Password.substring(Password.length() - 2));
				 
					for (i = 1; i <= intPwd; i++) {
						intTemp = 3 * i - 3; 
						strNew.append((char) (Integer.parseInt(strTemp.substring(intTemp, intTemp + 3))));
					}  

					decrypt = strNew.toString();
				} catch (Exception e) {
					e.printStackTrace();
				} 
				return decrypt;
			}

  
}
