package com.test.mq.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class HelloWorldController {
	private static final Logger logger = Logger.getLogger(HelloWorldController.class);
	@Autowired
	private JmsTemplate jmsTemplate;

	@RequestMapping(value="/send", method = RequestMethod.GET)
	public String sendMessage(@RequestParam(value = "message", defaultValue = "hello world") String message){
		String content = message;
		logger.info("Test send - Message received: " + message);
		Connection conn = getConnectionToDB();
		if(conn != null){
			 insertDB(conn, message);
		}else{
			 
		}
		jmsTemplate.convertAndSend("TESTQUEUE", message);
 
		return "welcome";
	}

	@RequestMapping(value="/hello", method = RequestMethod.GET)
	public String sayHello(ModelMap model) {
		model.addAttribute("greeting", "Hello World from Spring 4 MVC");
		return "welcome";
	}


	@RequestMapping(value="/helloagain", method = RequestMethod.GET)
	public String sayHelloAgain(ModelMap model) {
		model.addAttribute("greeting", "Hello World Again, from Spring 4 MVC");
		return "welcome";
	}
	
	public Connection getConnectionToDB() {
		Properties mainProperties = new Properties();
	    String jdbc = "";
	    String jdbcUser = "";
	    String jdbcPassword = "";
	    
	    try {
			FileInputStream file=new FileInputStream("D:\\MQListener\\config\\dbConnection.properties");
			mainProperties.load(file);
			file.close();
			jdbc = mainProperties.getProperty("jdbcConnection");
			jdbcUser = mainProperties.getProperty("jdbcUser");
			jdbcPassword = mainProperties.getProperty("jdbcPassword");
			
		} catch (FileNotFoundException e1) {
			//logger.error("Config file not exists !!!");
			e1.printStackTrace();
		} catch (IOException e) {
			//logger.error("Config file failed to load !!!");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

		} catch (ClassNotFoundException cnfe) {
			System.out.println("ClassNotFoundException");
		}
		
		try{
	
			Connection conn = DriverManager.getConnection(jdbc,jdbcUser,jdbcPassword);
			return conn;
			
		}catch(Exception e){
			return null;
		}
	}

	public String insertDB(Connection conn, String message){
		Statement stmt = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		
		Date createdDate = new Date();
		String strCurrDateTime = formatter.format(createdDate);
		
		String queryString = "Insert into DB.dbo.TB (queueMessage, createdDate) values ('"+message+"','"+strCurrDateTime+"')";
 
		try{
			stmt = conn.createStatement(); 
			stmt.executeUpdate(queryString);
			 
			return "inserted !!!! ";
		}catch(SQLException e){
			logger.error("Error insertDB -",e);
			return "errorrrrr";
		}
	} 
}
